;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :mct)
(in-readtable :qtools)


;;;--------------------------------------------------
;;;    DMS Service Dialog
;;;--------------------------------------------------
(define-widget dms-dialog (QDialog)
  ((command :reader command
	    :initform nil)
   (argument :reader argument
	     :initform nil)
   (destination-id :reader destination-id
	     :initform nil)))

(define-subwidget (dms-dialog combobox-description) (q+:make-qlabel "Command" dms-dialog))
(define-subwidget (dms-dialog status-combobox) (q+:make-qcombobox dms-dialog)
  (q+:add-item status-combobox "Reboot")
  (q+:add-item status-combobox "Delete")
  (q+:add-item status-combobox "Compile")
  (q+:add-item status-combobox "Compile&Delete")
  (q+:add-item status-combobox "Maintenance"))
(define-subwidget (dms-dialog argument-text) (q+:make-qlineedit dms-dialog)
  (q+:set-placeholder-text argument-text "Argument"))

(define-subwidget (dms-dialog destination-device-label) (q+:make-qlabel "Destination ID" dms-dialog))
(define-subwidget (dms-dialog destination-id-text) (q+:make-qlineedit dms-dialog)
  (q+:set-placeholder-text destination-id-text "Device ID"))

(define-subwidget (dms-dialog log-view) (q+:make-qtextedit dms-dialog)
  (q+:hide log-view))

(define-subwidget (dms-dialog ok-button) (q+:make-qpushbutton "Start" dms-dialog))
(define-subwidget (dms-dialog cancel-button) (q+:make-qpushbutton "Cancel" dms-dialog))

(define-subwidget (dms-dialog layout) (q+:make-qgridlayout dms-dialog)
  (q+:add-widget layout combobox-description 0 0)
  (q+:add-widget layout status-combobox 0 1)
  (q+:add-widget layout argument-text 0 2)
  (q+:add-widget layout destination-device-label 1 0)
  (q+:add-widget layout destination-id-text 1 1)
  (q+:add-widget layout log-view 2 0 2 3)
  (q+:add-widget layout ok-button 4 1)
  (q+:add-widget layout cancel-button 4 2))

(define-slot (dms-dialog ok-button) ()
  (declare (connected ok-button (pressed)))
  (signal! dms-dialog (accepted)))

(define-slot (dms-dialog accept) ()
  (declare (connected dms-dialog  (accepted)))

  (setf command (nth (q+:current-index status-combobox) '(:reboot :delete :compile :compile/delete :maintenance)))

  ;; @TODO Check validity of input.
  (when (or (not command)
	    (and (string= "" (q+:text argument-text))
		 (or (eq 0 command)
		     (eq 4 command)))
	    (string= "" (q+:text destination-id-text)))
    (let ((mb (q+:qmessagebox-information dms-dialog "Input validation failed" "Please verify all input data.")))
      (q+:add-button mb "Close" 1)
      (q+:exec mb)
      (return-from %dms-dialog-slot-accept)))	  
  
  (setf argument (q+:text argument-text))
  (setf destination-id (parse-integer (q+:text destination-id-text)))
  
  (q+:show log-view)
  (setf (q+:enabled ok-button) nil)
  (setf (q+:enabled cancel-button) nil)

  (let ((cb-id (add-callback nil)))
    (flet ((finalize-service ()
	     (remove-callback cb-id)
	     (q+:set-text ok-button "Repeat")
	     (q+:set-text cancel-button "Close")
	     (setf (q+:enabled ok-button) t)
	     (setf (q+:enabled cancel-button) t)))
      (set-callback cb-id
		    #'(lambda (code &rest args)
			(log:info code args)
			;; Update some GUI element to show the updated info
			;; Remove itself when all done
			(q+:append log-view
				   (case code
				     (:start-request-sent         "Start request sent")
				     (:start-request-resent       "Start request resent")
				     (:start-request-send-failure "Start request send failure")
				     (:start-response-received    (prog1
								      "Start response received"
								    (finalize-service)))
				     (t (format nil "Invalid response code '~a'" code))))))
      (signal! device-controller (service-request string string int)
	       "DMS"
	       ;; @REVIEW Since we cannot transmit a list in a signal, we have to serialize it...
	       (with-output-to-string (str)
		 (trivial-json-codec:serialize
		  (list :destination-id destination-id :command command :argument argument :source-id 1)
		  str))
	       cb-id))))

(define-slot (dms-dialog reject) ()
  (declare (connected dms-dialog  (rejected)))
  (q+:close dms-dialog))

(define-slot (dms-dialog cancel-button) ()
  (declare (connected cancel-button (pressed)))
  (q+:close dms-dialog))



;;;--------------------------------------------------
;;;    DIS Service Dialog
;;;--------------------------------------------------
(define-widget dis-dialog (QDialog)
  ((destination-sn :reader destination-sn
		   :initform nil)))

(define-subwidget (dis-dialog destination-sn-label) (q+:make-qlabel "Destination SN" dis-dialog))
(define-subwidget (dis-dialog destination-sn-text) (q+:make-qlineedit dis-dialog)
  (q+:set-placeholder-text destination-sn-text "Device Serial Number"))

(define-subwidget (dis-dialog ok-button) (q+:make-qpushbutton "Ok" dis-dialog))
(define-subwidget (dis-dialog cancel-button) (q+:make-qpushbutton "Cancel" dis-dialog))

(define-subwidget (dis-dialog layout) (q+:make-qgridlayout dis-dialog)
  (q+:add-widget layout destination-sn-label 0 0)
  (q+:add-widget layout destination-sn-text 0 1)
  (q+:add-widget layout ok-button 1 0)
  (q+:add-widget layout cancel-button 1 1))

(define-slot (dis-dialog ok-button) ()
  (declare (connected ok-button (pressed)))
  (signal! dis-dialog (accepted)))

(define-slot (dis-dialog accept) ()
  (declare (connected dis-dialog  (accepted)))
  
  ;; @TODO Check validity of input.

  (setf destination-sn (parse-integer (q+:text destination-sn-text)))  
  (q+:close dis-dialog))
  
(define-slot (dis-dialog reject) ()
  (declare (connected dis-dialog  (rejected)))
  (q+:close dis-dialog))

(define-slot (dis-dialog cancel-button) ()
  (declare (connected cancel-button (pressed)))
  (q+:close dis-dialog))

;;;--------------------------------------------------
;;;    ISS Service Dialog
;;;--------------------------------------------------
(define-widget iss-dialog (QDialog)
  ((destination-sn :reader destination-sn
		   :initform nil)
   (new-id :reader new-id
	   :initform nil)))

(define-initializer (iss-dialog setup)
  (setf (q+:window-title iss-dialog) "ID Setting Service"))

(define-subwidget (iss-dialog destination-sn-label) (q+:make-qlabel "Destination SN" iss-dialog))
(define-subwidget (iss-dialog destination-sn-text) (q+:make-qlineedit iss-dialog)
  (q+:set-placeholder-text destination-sn-text "Device Serial Number"))

(define-subwidget (iss-dialog new-id-label) (q+:make-qlabel "New ID" iss-dialog))
(define-subwidget (iss-dialog new-id-text) (q+:make-qlineedit iss-dialog)
  (q+:set-placeholder-text new-id-text "Device ID"))

(define-subwidget (iss-dialog ok-button) (q+:make-qpushbutton "Ok" iss-dialog))
(define-subwidget (iss-dialog cancel-button) (q+:make-qpushbutton "Cancel" iss-dialog))

(define-subwidget (iss-dialog layout) (q+:make-qgridlayout iss-dialog)
  (q+:add-widget layout destination-sn-label 0 0)
  (q+:add-widget layout destination-sn-text 0 1)
  (q+:add-widget layout new-id-label 1 0)
  (q+:add-widget layout new-id-text 1 1)
  (q+:add-widget layout ok-button 2 0)
  (q+:add-widget layout cancel-button 2 1))

(define-slot (iss-dialog ok-button) ()
  (declare (connected ok-button (pressed)))
  (signal! iss-dialog (accepted)))

(define-slot (iss-dialog accept) ()
  (declare (connected iss-dialog  (accepted)))
  
  ;; @TODO Check validity of input.

  (setf destination-sn (parse-integer (q+:text destination-sn-text)))
  (setf new-id (parse-integer (q+:text new-id-text)))
  (q+:close iss-dialog))
  
(define-slot (iss-dialog reject) ()
  (declare (connected iss-dialog  (rejected)))
  (q+:close iss-dialog))

(define-slot (iss-dialog cancel-button) ()
  (declare (connected cancel-button (pressed)))
  (q+:close iss-dialog))


;;;--------------------------------------------------
;;;    PIS Service Dialog
;;;--------------------------------------------------
(define-widget pis-dialog (QDialog)
  ((parameter :reader parameter
	    :initform nil)
   (value :reader value
	  :initform nil)
   (write-value :reader write-value
		:initform nil)
   (destination-id :reader destination-id
	     :initform nil)))

(define-initializer (pis-dialog setup)
  (setf (q+:window-title pis-dialog) "Parameter Setting Service"))

(define-subwidget (pis-dialog destination-id-label) (q+:make-qlabel "Destination ID" pis-dialog))
(define-subwidget (pis-dialog destination-id-text) (q+:make-qlineedit pis-dialog)
  (q+:set-placeholder-text destination-id-text "Device ID"))
(define-subwidget (pis-dialog write-parameter-check) (q+:make-qcheckbox "Write" pis-dialog))

(define-subwidget (pis-dialog parameter-label) (q+:make-qlabel "Parameter" pis-dialog))
(define-subwidget (pis-dialog parameter-text) (q+:make-qlineedit pis-dialog)
  (q+:set-placeholder-text parameter-text "Parameter name"))
(define-subwidget (pis-dialog parameter-value-text) (q+:make-qlineedit pis-dialog)
  (q+:set-placeholder-text parameter-value-text "Parameter value"))

(define-subwidget (pis-dialog ok-button) (q+:make-qpushbutton "Ok" pis-dialog))
(define-subwidget (pis-dialog cancel-button) (q+:make-qpushbutton "Cancel" pis-dialog))

(define-subwidget (pis-dialog layout) (q+:make-qgridlayout pis-dialog)
  (q+:add-widget layout destination-id-label 0 0)
  (q+:add-widget layout destination-id-text 0 1)
  (q+:add-widget layout write-parameter-check 0 2)
  (q+:add-widget layout parameter-label 1 0)
  (q+:add-widget layout parameter-text 1 1)
  (q+:add-widget layout parameter-value-text 1 2)
  (q+:add-widget layout ok-button 2 1)
  (q+:add-widget layout cancel-button 2 2))

(define-slot (pis-dialog ok-button) ()
  (declare (connected ok-button (pressed)))
  (signal! pis-dialog (accepted)))

(define-slot (pis-dialog accept) ()
  (declare (connected pis-dialog  (accepted)))
  
  ;; @TODO Check validity of input.

  (setf destination-id (parse-integer (q+:text destination-id-text)))
  (setf parameter (q+:text parameter-text))
  (setf value (q+:text parameter-value-text))
  (setf write-value (> (q+:check-state write-parameter-check) 0))
  (q+:close pis-dialog))
  
(define-slot (pis-dialog reject) ()
  (declare (connected pis-dialog  (rejected)))
  (q+:close pis-dialog))

(define-slot (pis-dialog cancel-button) ()
  (declare (connected cancel-button (pressed)))
  (q+:close pis-dialog))


;;;--------------------------------------------------
;;;    DUS Service Dialog
;;;--------------------------------------------------
(define-widget dus-dialog (QDialog)
  ((destination-id :reader destination-id
		   :initform nil)
   (file-name :reader file-name
	      :initform nil)
   (contents-path :reader contents-path
		  :initform nil)))

(define-initializer (dus-dialog setup)
  (setf (q+:window-title dus-dialog) "Data Upload Service"))


(define-subwidget (dus-dialog destination-id-label) (q+:make-qlabel "Destination ID" dus-dialog))
(define-subwidget (dus-dialog destination-id-text) (q+:make-qlineedit dus-dialog)
  (q+:set-placeholder-text destination-id-text "Device ID"))

(define-subwidget (dus-dialog destination-filename-label) (q+:make-qlabel "File name" dus-dialog))
(define-subwidget (dus-dialog destination-filename-text) (q+:make-qlineedit dus-dialog)
  (q+:set-placeholder-text destination-filename-text "File name on Device"))

(define-subwidget (dus-dialog destination-file-label) (q+:make-qlabel "File" dus-dialog))
(define-subwidget (dus-dialog destination-file-text) (q+:make-qlineedit dus-dialog)
  (q+:set-placeholder-text destination-file-text "File"))
(define-subwidget (dus-dialog open-file-button) (q+:make-qpushbutton "Select" dus-dialog))

(define-subwidget (dus-dialog log-view) (q+:make-qtextedit dus-dialog)
  (q+:hide log-view))

(define-subwidget (dus-dialog ok-button) (q+:make-qpushbutton "Ok" dus-dialog))
(define-subwidget (dus-dialog cancel-button) (q+:make-qpushbutton "Cancel" dus-dialog))

(define-subwidget (dus-dialog layout) (q+:make-qgridlayout dus-dialog)
  (q+:add-widget layout destination-id-label 0 0)
  (q+:add-widget layout destination-id-text 0 1)
  (q+:add-widget layout destination-filename-label 1 0)
  (q+:add-widget layout destination-filename-text 1 1)
  (q+:add-widget layout destination-file-label 2 0)
  (q+:add-widget layout destination-file-text 2 1)
  (q+:add-widget layout open-file-button 2 2)
  (q+:add-widget layout log-view 3 0 2 3)
  (q+:add-widget layout ok-button 5 1)
  (q+:add-widget layout cancel-button 5 2))

(define-slot (dus-dialog open-file-button) ()
  (declare (connected open-file-button (pressed)))
  (let ((dialog (q+:make-qfiledialog)))
    (q+:set-directory dialog
		      (uiop:native-namestring
		       (merge-pathnames "../"
					(asdf:system-source-directory :mct))))
    (q+:exec dialog)

    (let ((file (car (q+:selected-files dialog))))
      (when (or (null (q+:text destination-filename-text))
		(string= (q+:text destination-filename-text) ""))
	(q+:insert destination-filename-text
		   (concatenate 'string (pathname-name file) "." (pathname-type file))))
      
      (q+:insert destination-file-text (namestring file)))))

(define-slot (dus-dialog ok-button) ()
  (declare (connected ok-button (pressed)))
  (signal! dus-dialog (accepted)))

(define-slot (dus-dialog accept) ()
  (declare (connected dus-dialog  (accepted)))

  ;; @TODO Check validity of input.
  (when (or (string= "" (q+:text destination-filename-text))
	    (> (length (q+:text destination-filename-text)) 20)
	    (string= "" (q+:text destination-file-text))
	    (string= "" (q+:text destination-id-text)))
    (let ((mb (q+:qmessagebox-information dus-dialog
					  "Input validation failed"
					  "Please verify all input data.")))
      (q+:add-button mb "Close" 1)
      (return-from %dus-dialog-slot-accept)))

  (setf destination-id (parse-integer (q+:text destination-id-text)))
  (setf file-name (q+:text destination-filename-text))
  (setf contents-path (q+:text destination-file-text))
  
  (q+:show log-view)
  (setf (q+:enabled ok-button) nil)
  (setf (q+:enabled cancel-button) nil)

  (let ((cb-id (add-callback nil)))
    (flet ((finalize-service ()
	     (remove-callback cb-id)
	     (q+:set-text ok-button "Repeat")
	     (q+:set-text cancel-button "Close")
	     (setf (q+:enabled ok-button) t)
	     (setf (q+:enabled cancel-button) t)))
      (set-callback cb-id
		    #'(lambda (code &rest args)
			;; Update some GUI element to show the updated info
			;; Remove itself when all done
			(q+:append log-view
				   (case code
				     (:start-request-sent         "Start request sent")
				     (:start-request-resent       "Start request resent")
				     (:start-request-send-failure (prog1
								      "Start request send failure"
								    (finalize-service)))
				     (:start-response-received    (let ((code (communication-protocol:error-code
									       (communication-protocol:payload (getf args :message)))))
								    (unless (eq 0 code)
								      (finalize-service))
								    (format nil "Start response received with code ~a" code)))
				     (:data-request-sent          (format nil "Data request sent frame ~a" (getf args :frame)))
				     (:data-request-resent        (format nil "Data request resent frame ~a" (getf args :frame)))
				     (:data-request-send-failure  (prog1
								      "Data request failure"
								    (finalize-service)))
				     (:data-upload-complete       (prog1
								      "Data upload successful"
								    (finalize-service)))
				     (t (format nil "Invalid response code '~a'" code))))))
      (signal! device-controller (service-request string string int)
	       "DUS"
	       ;; @REVIEW Since we cannot transmit a list in a signal, we have to serialize it...
	       (with-output-to-string (str)
		 (trivial-json-codec:serialize
		  (list :destination-id destination-id :file-name file-name :contents-path contents-path :source-id 1)
		  str))
	       cb-id))))

(define-slot (dus-dialog reject) ()
  (declare (connected dus-dialog  (rejected)))
  (q+:close dus-dialog))

(define-slot (dus-dialog cancel-button) ()
  (declare (connected cancel-button (pressed)))
  (q+:close dus-dialog))


;;;--------------------------------------------------
;;;    DDS Service Dialog
;;;--------------------------------------------------
(define-widget dds-dialog (QDialog)
  ((destination-id :reader destination-id
		   :initform nil)
   (file-name :reader file-name
	      :initform nil)
   (contents-path :reader contents-path
	   :initform nil)))


;;;--------------------------------------------------
;;;    Update Dialog
;;;--------------------------------------------------
(define-widget update-dialog (QDialog)
  ((destination-id :reader destination-id
		   :initform nil)
   (update-folder :reader update-folder
		  :initform nil)))

(define-initializer (update-dialog setup)
  (setf (q+:window-title update-dialog) "Device Update"))

(define-subwidget (update-dialog destination-id-label) (q+:make-qlabel "Destination ID" update-dialog))
(define-subwidget (update-dialog destination-id-text) (q+:make-qlineedit update-dialog)
  (q+:set-placeholder-text destination-id-text "Device ID"))

(define-subwidget (update-dialog destination-file-label) (q+:make-qlabel "Update Folder" update-dialog))
(define-subwidget (update-dialog destination-file-text) (q+:make-qlineedit update-dialog)
  (q+:set-placeholder-text destination-file-text "Folder containing update files"))
(define-subwidget (update-dialog open-file-button) (q+:make-qpushbutton "Select" update-dialog))

(define-subwidget (update-dialog log-view) (q+:make-qtextedit update-dialog)
  (q+:hide log-view))

(define-subwidget (update-dialog ok-button) (q+:make-qpushbutton "Ok" update-dialog))
(define-subwidget (update-dialog cancel-button) (q+:make-qpushbutton "Cancel" update-dialog))

(define-subwidget (update-dialog layout) (q+:make-qgridlayout update-dialog)
  (q+:add-widget layout destination-id-label 0 0)
  (q+:add-widget layout destination-id-text 0 1)
  (q+:add-widget layout destination-file-label 1 0)
  (q+:add-widget layout destination-file-text 1 1)
  (q+:add-widget layout open-file-button 1 2)
  (q+:add-widget layout log-view 2 0 2 3)
  (q+:add-widget layout ok-button 4 1)
  (q+:add-widget layout cancel-button 4 2))

(define-slot (update-dialog open-file-button) ()
  (declare (connected open-file-button (pressed)))
  (let ((dialog (q+:qfiledialog-get-existing-directory)))     
      (q+:insert destination-file-text (namestring dialog))))

(define-slot (update-dialog ok-button) ()
  (declare (connected ok-button (pressed)))
  (signal! update-dialog (accepted)))

(defclass command ()
  ((service-code :initarg :service-code
		 :reader service-code)
   (service-arg :initarg :service-arg
		:reader service-arg)
   (callback :initarg :cb
	     :reader callback)
   (log-entry :initarg :log-entry
	      :reader log-entry)))

(define-slot (update-dialog accept) ()
  (declare (connected update-dialog  (accepted)))

  ;; @TODO Check validity of input.
  (when (or (string= "" (q+:text destination-file-text))
	    (string= "" (q+:text destination-id-text)))
    (let ((mb (q+:qmessagebox-information update-dialog
					  "Input validation failed"
					  "Please verify all input data.")))
      (q+:add-button mb "Close" 1)
      (return-from %update-dialog-slot-accept)))

  (setf destination-id (parse-integer (q+:text destination-id-text)))
  (setf update-folder (q+:text destination-file-text))
  
  (q+:show log-view)
  (setf (q+:enabled ok-button) nil)
  (setf (q+:enabled cancel-button) nil)

  (let ((commands nil)
	(cb-id (add-callback nil)))
  (labels ((finalize-request ()
	     (remove-callback cb-id)
	     (q+:set-text ok-button "Repeat")
	     (q+:set-text cancel-button "Close")
	     (setf (q+:enabled ok-button) t)
	     (setf (q+:enabled cancel-button) t))
	   (call-next-command ()
	     (if commands
		 (progn
		   (let ((command (car commands)))
		     (q+:append log-view (log-entry command))
		     (setf commands (cdr commands))
		     (set-callback cb-id (callback command))
		     (signal! device-controller (service-request string string int)
			      (service-code command)
			      (service-arg command)
			      cb-id)))
		 (progn
		   (finalize-request)))))

    ;; Put the device into Maintenance Mode
    ;;(push (make-instance 'command
;;			 :log-entry "Putting the device into Maintenance Mode..."
;;			 :service-code "DMS"
;;			 :service-arg (with-output-to-string (str)
;;					(trivial-json-codec:serialize
;;					 (list :destination-id destination-id :command :maintenance :argument "" :source-id 1)
;;					 str))
;;			 :cb #'(lambda (code &rest args)
;;				 (declare (ignore args))
;;				 (case code
;;				   (:start-request-send-failure (q+:append log-view "... failed")
;;								(finalize-request))
;;				   (:start-response-received    (q+:append log-view "... done")
;;								;; Wait for the reboot confirmation message 
;;								(call-next-command)))))
;;	  commands)

    ;; Create a DUS command for each file in the upload folder (renaming it)
    (iterate:iterate
      (iterate:for file in (directory (make-pathname
				       :name :wild
				       :type :wild
				       :directory update-folder)))
      (let ((file-name (concatenate 'string (pathname-name file) "." (pathname-type file))))
	(push (make-instance 'command
			     :log-entry (concatenate 'string "Uploading " file-name "...")
			     :service-code "DUS"
			     :service-arg (with-output-to-string (str)
					    (trivial-json-codec:serialize
					     (list :destination-id destination-id :file-name (concatenate 'string "__" file-name) :contents-path (namestring file) :source-id 1)
					     str))
			     :cb #'(lambda (code &rest args)
				     (declare (ignore args))
				     (case code
				       (:start-request-send-failure (q+:append log-view "... failed")
								    (finalize-request))
				       (:data-request-send-failure (q+:append log-view "... failed")
								   (finalize-request))
				       (:data-upload-complete    (q+:append log-view "... done")
								 (call-next-command)))))
	      commands)))

    ;; Finally restart
    (push (make-instance 'command
			 :log-entry "Restarting the device to trigger updating..."
			 :service-code "DMS"
			 :service-arg (with-output-to-string (str)
					(trivial-json-codec:serialize
					 (list :destination-id destination-id :command :reboot :argument "" :source-id 1)
					 str))
			 :cb #'(lambda (code &rest args)
				 (declare (ignore args))
				 (case code
				   (:start-request-send-failure (q+:append log-view "... failed")
								(finalize-request))
				   (:start-response-received    (q+:append log-view "... done")
								(call-next-command)))))
	  commands)

    (setf commands (reverse commands))
    (call-next-command))))

(define-slot (update-dialog reject) ()
  (declare (connected update-dialog  (rejected)))
  (q+:close update-dialog))

(define-slot (update-dialog cancel-button) ()
  (declare (connected cancel-button (pressed)))
  (q+:close update-dialog))

