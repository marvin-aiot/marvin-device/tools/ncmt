;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :mct)
(in-readtable :qtools)

(defparameter main nil)


;;;--------------------------------------------------
;;;    Message Control Widget
;;;--------------------------------------------------
(define-widget message-control (QWidget)
  ())

(define-subwidget (message-control freeze-button) (q+:make-qpushbutton "Freeze" message-control))
(define-subwidget (message-control filter-button) (q+:make-qpushbutton "Filter" message-control))
(define-subwidget (message-control clear-button) (q+:make-qpushbutton "Clear" message-control))
(define-subwidget (message-control export-button) (q+:make-qpushbutton "Export" message-control))

(define-subwidget (message-control layout) (q+:make-qhboxlayout message-control)
  (q+:add-widget layout freeze-button)
  (q+:add-widget layout filter-button)
  (q+:add-widget layout clear-button)
  (q+:add-widget layout export-button))

(define-slot (message-control freeze-messages) ()
  (declare (connected freeze-button (pressed)))
  (log:info "Messages freezed."))

(define-slot (message-control filter-messages) ()
  (declare (connected filter-button (pressed)))
  (log:info "Messages filtered."))

(define-slot (message-control clear-messages) ()
  (declare (connected clear-button (pressed)))
  (log:info "Messages cleared."))

(define-slot (message-control export-messages) ()
  (declare (connected export-button (pressed)))
  (log:info "Messages exported."))

;;;--------------------------------------------------
;;;    Message List Widget
;;;--------------------------------------------------
(define-widget message-list (QTableView)
  ())

;;;--------------------------------------------------
;;;    Message View Widget
;;;--------------------------------------------------
(define-widget message-view (QWidget)
  ())

(define-subwidget (message-view message-list) (q+:make-qtableview message-view)
  (q+:set-model message-list messages)
  (q+:set-column-width message-list 0 160)
  (q+:set-column-width message-list 1 100)
  (q+:set-column-width message-list 2 80)
  (q+:set-column-width message-list 3 80)
  (q+:set-column-width message-list 4 80)
  (q+:set-column-width message-list 5 40)
  (q+:set-column-width message-list 6 40)
  (q+:set-column-width message-list 7 80))

(define-subwidget (message-view message-control) (make-instance 'message-control))

(define-subwidget (message-view layout) (q+:make-qvboxlayout message-view)
  (q+:add-widget layout message-list)
  (q+:add-widget layout message-control))


;;;--------------------------------------------------
;;;    Device List Widget
;;;--------------------------------------------------
(define-widget device-list (QWidget)
  ())

(define-subwidget (device-list known-devices-list) (q+:make-qtableview device-list)
  (q+:set-model known-devices-list known-devices)
  (q+:set-column-width known-devices-list 0 50)
  (q+:set-column-width known-devices-list 1 100)
  (q+:set-column-width known-devices-list 2 100)
  (q+:set-column-width known-devices-list 3 100))

(define-subwidget (device-list discover-button) (q+:make-qpushbutton "Discover" device-list))

(define-subwidget (device-list layout) (q+:make-qvboxlayout device-list)
  (q+:add-widget layout known-devices-list)
  (q+:add-widget layout discover-button))

(define-slot (device-list discover-devices) ()
  (declare (connected discover-button (pressed)))
  (log:info "Discover devices."))


;;;--------------------------------------------------
;;;    Device Control Widget
;;;--------------------------------------------------
(define-widget device-control (QWidget)
  ())

(define-subwidget (device-control send-button) (q+:make-qpushbutton "Send Event" device-control))
(define-subwidget (device-control dms-button) (q+:make-qpushbutton "DMS" device-control))
(define-subwidget (device-control dis-button) (q+:make-qpushbutton "DIS" device-control))
(define-subwidget (device-control iss-button) (q+:make-qpushbutton "ISS" device-control))
(define-subwidget (device-control pis-button) (q+:make-qpushbutton "PIS" device-control))
(define-subwidget (device-control dus-button) (q+:make-qpushbutton "DUS" device-control))
(define-subwidget (device-control dds-button) (q+:make-qpushbutton "DDS" device-control))
(define-subwidget (device-control update-button) (q+:make-qpushbutton "Update" device-control))

(define-subwidget (device-control layout) (q+:make-qvboxlayout device-control)
  (q+:set-contents-margins layout 50 11 50 11)
  (q+:add-widget layout send-button)
  (q+:add-widget layout dms-button)
  (q+:add-widget layout dis-button)
  (q+:add-widget layout iss-button)
  (q+:add-widget layout pis-button)
  (q+:add-widget layout dus-button)
  (q+:add-widget layout dds-button)
  (q+:add-widget layout update-button))


(define-slot (device-control send-message) ()
  (declare (connected send-button (pressed)))
  (log:info "Send message."))

(define-slot (device-control start-dms-service) ()
  (declare (connected dms-button (pressed)))
  (let ((dialog (make-instance 'dms-dialog)))
    (q+:set-modal dialog t)
    (q+:exec dialog)))

(define-slot (device-control start-dis-service) ()
  (declare (connected dis-button (pressed)))
  (log:info "Starting DIS.")
  (let ((dialog (make-instance 'dis-dialog)))
    (q+:set-modal dialog t)
    (q+:exec dialog)

    (when (destination-sn dialog)
      (signal! device-controller (service-request string string int)
	       "DIS"
	       ;; @REVIEW Since we cannot transmit a list in a signal, we have to serialize it...
	       (with-output-to-string (str)
		 (trivial-json-codec:serialize
		  (list :destination-sn (destination-sn dialog) :source-id 1)
		  str))
	       (add-callback #'(lambda (code &rest args)
				 (log:info code args)))))))

(define-slot (device-control start-iss-service) ()
  (declare (connected iss-button (pressed)))
  (log:info "Starting ISS.")
  (let ((dialog (make-instance 'iss-dialog)))
    (q+:set-modal dialog t)
    (q+:exec dialog)

    (when (destination-sn dialog)
      (signal! device-controller (service-request string string int)
	       "ISS"
	       ;; @REVIEW Since we cannot transmit a list in a signal, we have to serialize it...
	       (with-output-to-string (str)
		 (trivial-json-codec:serialize
		  (list :destination-sn (destination-sn dialog) :new-id (new-id dialog) :source-id 1)
		  str))
	       (add-callback #'(lambda (code &rest args)
				 (log:info code args)))))))

(define-slot (device-control start-pis-service) ()
  (declare (connected pis-button (pressed)))
  (let ((dialog (make-instance 'pis-dialog)))
    (q+:set-modal dialog t)
    (q+:exec dialog)

    (when (parameter dialog)
      (signal! device-controller (service-request string string int)
	       "PIS"
	       ;; @REVIEW Since we cannot transmit a list in a signal, we have to serialize it...
	       (with-output-to-string (str)
		 (trivial-json-codec:serialize
		  (list :destination-id (destination-id dialog) :parameter (parameter dialog) :value (value dialog) :write (write-value dialog) :source-id 1)
		  str))
	       (add-callback #'(lambda (code &rest args)
				 (log:info code args)))))))

(define-slot (device-control start-dus-service) ()
  (declare (connected dus-button (pressed)))
  (log:info "Starting DUS.")
  (let ((dialog (make-instance 'dus-dialog)))
    (q+:set-modal dialog t)
    (q+:exec dialog)))

(define-slot (device-control start-dds-service) ()
  (declare (connected dds-button (pressed)))
  (log:info "Starting DDS."))

(define-slot (device-control start-update) ()
  (declare (connected update-button (pressed)))
  (log:info "Starting Device Update.")
  (let ((dialog (make-instance 'update-dialog)))
    (q+:set-modal dialog t)
    (q+:exec dialog)))

;;;--------------------------------------------------
;;;    Device View Widget
;;;--------------------------------------------------
(define-widget device-view (QWidget)
  ())

(define-subwidget (device-view device-list) (make-instance 'device-list))
(define-subwidget (device-view device-control) (make-instance 'device-control))

(define-subwidget (device-view layout) (q+:make-qvboxlayout device-view)
  (q+:add-widget layout device-list)
  (q+:add-widget layout device-control))


;;;--------------------------------------------------
;;;    Device View Widget
;;;--------------------------------------------------
(define-widget main-layout (QWidget)
  ())

(define-subwidget (main-layout message-view) (make-instance 'message-view))
(define-subwidget (main-layout device-view) (make-instance 'device-view))

(define-subwidget (main-layout layout) (q+:make-qgridlayout main-layout)
  (setf (q+:column-stretch layout 0) 1)
  (q+:add-widget layout message-view 0 0)
  (q+:add-widget layout device-view 0 1))


;;;--------------------------------------------------
;;;    Main Window
;;;--------------------------------------------------
(define-widget main-window (QMainWindow)
  ())

(define-initializer (main-window setup)
  (setf (q+:window-title main-window) "Marvin: Management & Configuration Tool")
  (trivial-monitored-thread:start-thread-monitor)
  (initialize-communication)
  (q+:resize main-window 800 600)
  (setf main main-window)
  (setf device-controller (make-instance 'controller)))

(define-subwidget (main-window layout) (make-instance 'main-layout)
  (setf (q+:central-widget main-window) layout))

(define-finalizer (main-window teardown)
  (setf device-controller nil)
  (setf main nil))


;;;--------------------------------------------------
;;;    Menu and menu functions
;;;--------------------------------------------------
(defun die ()
  (q+:close main))

(define-menu (main-window File)
  (:item ("Quit" (ctrl q)) (die)))

