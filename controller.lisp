;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :mct)
(in-readtable :qtools)

(defparameter callbacks '())
(defparameter device-controller nil)

(defun add-callback (callback)
  (when (> (length callbacks) 500000)
    (log:warn "Callback queue growing too large."))
  
  (add-callback% callback))

(defun add-callback% (callback)
  ;;Returns an id
  (let ((id (random 999999)))
    (unless (member id callbacks :key #'car)
      (push (cons id callback) callbacks)
      (return-from add-callback% id)))
  (add-callback% callback))

(defun set-callback (id callback)
  (trivial-utilities:aif (assoc id callbacks)
			 (setf (cdr it) callback)
			 (cerror "Return NIL" "No registered callback with id '~a'." id)))

(defun get-callback (id &optional default)
  (trivial-utilities:aif (cdr (assoc id callbacks))
			 it
			 default))
  
(defun remove-callback (id)
  (setf callbacks (delete id callbacks :key #'car)))

(define-widget controller (QObject)
  ())

(define-signal (controller service-request) (string string int))

(define-slot (controller service-request) ((service string) (arguments string) (callback-id int))
  (declare (connected controller (service-request string string int)))
  (log:info service arguments)

  (let ((args (trivial-json-codec:deserialize-json arguments))
	(callback (get-callback callback-id #'communication-protocol:ignore-callback)))
    (alexandria:switch (service :test equal)
      ("DMS" (handle-dms-service args callback))
      ("DIS" (handle-dis-service args callback))
      ("ISS" (handle-iss-service args callback))
      ("PIS" (handle-pis-service args callback))
      ("DUS" (handle-dus-service args callback))
      ("DDS" (handle-dds-service args callback))
      (t (log:error "No service handler for '~a' defined!" service)))))

(defun send-message (message on-complete-cb)
  (communication-protocol:send-message message on-complete-cb))

(defun handle-dms-service (args callback)
  (let ((dest-id (getf args :destination-id))
	(cmd (getf args :command))
	(arg (getf args :argument))
	(source (getf args :source-id)))
    (communication-protocol:initiate-dms-service dest-id cmd :argument arg :source-id source :callback callback)))

(defun handle-dis-service (args callback)
  (let ((dest-sn (getf args :destination-sn))
	(source (getf args :source-id)))
    (communication-protocol:initiate-dis-service dest-sn :source-id source :callback callback)))

(defun handle-iss-service (args callback)
  (let ((dest-sn (getf args :destination-sn))
	(id (getf args :new-id))
	(source (getf args :source-id)))
    (communication-protocol:initiate-iss-service dest-sn id :source-id source :callback callback)))

(defun handle-pis-service (args callback)
  (let ((dest-id (getf args :destination-id))
	(param (getf args :parameter))
	(val (getf args :value))
	(write (getf args :write))
	(source (getf args :source-id)))
    (communication-protocol:initiate-pis-service dest-id param :value val :write write :source-id source :callback callback)))

(defun handle-dus-service (args callback)
  (let ((dest-id (getf args :destination-id))
	(file-name (getf args :file-name))
	(source (getf args :source-id))
	(contents (with-output-to-string (str)
		    (with-open-file (stream (getf args :contents-path) :direction :input :if-does-not-exist nil)
		      (unless stream
			(return-from handle-dus-service))
		      (iterate:iterate
			(iterate:for line = (read-line stream nil))
			(iterate:while line)
			(iterate:if-first-time nil (format str "~%"))
			(format str "~a" line))))))
    (communication-protocol:initiate-dus-service dest-id file-name contents :source-id source :callback callback)))

(defun handle-dds-service (args callback)
  (let ((dest-id (getf args :destination-id))
	(file-name (getf args :file-name))
	(source (getf args :source-id)))
    (communication-protocol:initiate-dds-service dest-id file-name :source-id source :callback callback)))
