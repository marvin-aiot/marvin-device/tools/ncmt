;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(defsystem :mct
  :name "Marvin Management & Configuration Tool"
  :description ""
  :long-description ""
  :version "0.0.1"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "GPL-3.0-or-later"
  :depends-on (:cl-annot
	       :qtools
	       :qtcore
	       :qtgui 
	       :qtopengl
               :uiop
	       :bordeaux-threads
	       :communication-protocol)
  :serial t
  :components ((:file "package")
	       (:file "controller")
	       (:file "model")
	       (:file "service-dialogs")
	       (:file "widgets")
	       (:file "communication")
	       (:file "mct")))
