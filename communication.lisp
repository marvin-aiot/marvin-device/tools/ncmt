;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :mct)

(defun new-message (message value)
  (push-message (make-instance 'message
			       :timestamp (get-universal-time)
			       :uid (communication-protocol:uid message)
			       :message-id (communication-protocol:message-id message)
			       :source-id (communication-protocol:source-id message)
			       :extended-info (communication-protocol:extended-info message)
			       :mode (communication-protocol:mode message)
			       :rtr (communication-protocol:rtr message)
			       :value value)))

(defun initialize-communication ()
  (communication-protocol:initialize-communication)
  (communication-protocol:set-incomming-message-callback #'new-message))
