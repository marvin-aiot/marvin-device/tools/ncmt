;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :mct)
(in-readtable :qtools)

(defparameter *font* (q+:make-qfont "Menlo" 12))
(defparameter *current-instruction-brush* (q+:make-qbrush (q+:make-qcolor 216 162 223)))

(defun model-index (model row col)
  (q+:index model row col (q+:make-qmodelindex)))

(defun data-changed! (model index-from &optional (index-to index-from))
  (signal! model (data-changed "QModelIndex" "QModelIndex")
           index-from
	   index-to))

;;;---------------------------------------------------
;;; Data Model for Known Devices
;;;---------------------------------------------------
(defclass device ()
  ((id :initarg :id
       :accessor id)
   (serial-number :initarg :serial-number
		  :accessor serial-number)
   (dev-type :initarg :dev-type
	 :accessor dev-type)
   (state :initarg :state
	  :accessor state)
   (description :initarg :description
		:accessor description)))

(defvar device-slots 5 "Number of slots in class 'device'")

(define-widget known-devices-data-model (QAbstractTableModel)
  ((known-devices :initarg :known-devices
		  :initform '()
		  :accessor known-devices)))


(define-override (known-devices-data-model column-count) (index)
  (declare (ignore index))
  device-slots)

(define-override (known-devices-data-model row-count) (index)
  (declare (ignore index))
  (length (known-devices known-devices-data-model)))

(define-override (known-devices-data-model data) (index role)
  (let ((row (q+:row index))
        (col (q+:column index)))
    (if (not (and (q+:is-valid index)
		  (< row (length (known-devices known-devices-data-model)))))
      (q+:make-qvariant)
      (qtenumcase role
        ((q+:qt.display-role)
         (let ((device (nth row (known-devices known-devices-data-model))))
	   (case col
	     (0 (id device))
	     (1 (serial-number device))
	     (2 (dev-type device))
	     (3 (format nil "~a" (state device)))
	     (4 (description device)))))

        ((q+:qt.font-role) *font*)

        ((q+:qt.background-role)
         (if (evenp row)
	     *current-instruction-brush*
	     (q+:make-qvariant)))

        ((q+:qt.text-alignment-role) #x0084)

	(t (q+:make-qvariant))))))

(define-override (known-devices-data-model header-data) (section orientation role)
  (case role
    (0 (qtenumcase orientation
         ((q+:qt.vertical) (q+:make-qvariant))
         ((q+:qt.horizontal) (case section
                               (0 "Id")
                               (1 "Serial Number")
                               (2 "Device Type")
                               (3 "State")
			       (4 "Description")))))
    (t (q+:make-qvariant))))


(defparameter known-devices (make-instance 'known-devices-data-model))

(defun add-device (device)
  
  (q+:begin-insert-rows known-devices (q+:make-qmodelindex) 0 0)
  (setf (known-devices known-devices)
	(sort (push device (known-devices known-devices))
	      #'< :key #'id))  
  (q+:end-insert-rows known-devices)
  (data-changed! known-devices (q+:make-qmodelindex)))

(defun update-device (device &key dev-type serial-number state description)

  (let ((row (position device (known-devices known-devices))))
    (when dev-type
      (setf (dev-type device) dev-type))
    (when serial-number
      (setf (serial-number device) serial-number))
    (when state
      (setf (state device) state))
    (when description
      (setf (description device) description))

  (data-changed! known-devices (model-index known-devices row 0) (model-index known-devices row device-slots))))


;;;---------------------------------------------------
;;; Data Model for Messages
;;;---------------------------------------------------
(defclass message ()
  ((timestamp :initarg :timestamp
	      :reader timestamp)
   (uid :initarg :uid
	:reader uid)
   (message-id :initarg :message-id
	       :type fixnum
	       :reader message-id)
   (source-id :initarg :source-id
	      :type fixnum
	      :reader source-id)
   (extended-info :initarg :extended-info
		  :reader extended-info)
   (mode :initarg :mode
	 :reader mode)
   (rtr :initarg :rtr
	:reader rtr)
   (value :initarg :value
	  :reader value)))

(defvar message-slots 8 "Number of slots in class 'message'")

(define-widget messages-data-model (QAbstractTableModel)
  ((messages :initarg :messages
	     :initform '()
	     :accessor messages)))

(define-override (messages-data-model column-count) (index)
  (declare (ignore index))
  message-slots)

(define-override (messages-data-model row-count) (index)
  (declare (ignore index))
  (length (messages messages-data-model)))

(define-override (messages-data-model data) (index role)
  (let ((row (q+:row index))
        (col (q+:column index)))
    (if (not (and (q+:is-valid index)
		  (< row (length (messages messages-data-model)))))
      (q+:make-qvariant)
      (qtenumcase role
        ((q+:qt.display-role)
         (let ((message (nth row (messages messages-data-model))))
	   (case col
	     (0 (multiple-value-bind (second minute hour day month year day-of-week dst-p tz)
		    (decode-universal-time (timestamp message))
		  (declare (ignore day-of-week dst-p tz))
		  (format nil "~d-~2,'0d-~2,'0d ~2,'0d:~2,'0d:~2,'0d"
			  year month day hour minute second)))
	     (1 (uid message))
	     (2 (message-id message))
	     (3 (source-id message))
	     (4 (extended-info message))
	     (5 (mode message))
	     (6 (rtr message))
	     (7 (value message)))))

        ((q+:qt.font-role) *font*)

        ((q+:qt.background-role)
         (if (evenp row)
	     *current-instruction-brush*
	     (q+:make-qvariant)))

        ((q+:qt.text-alignment-role) #x0084)

	(t (q+:make-qvariant))))))

(define-override (messages-data-model header-data) (section orientation role)
  (case role
    (0 (qtenumcase orientation
         ((q+:qt.vertical) (q+:make-qvariant))
         ((q+:qt.horizontal) (case section
			       (0 "Timestamp")
                               (1 "UID")
                               (2 "Message Id")
                               (3 "Source Id")
                               (4 "Ext. Info")
                               (5 "Mode")
                               (6 "RTR")
                               (7 "Value")))))
    (t (q+:make-qvariant))))


(defparameter messages (make-instance 'messages-data-model))

(defun add-message (message)
  (q+:begin-insert-rows messages (q+:make-qmodelindex) 0 0)
  (push message (messages messages))
  (q+:end-insert-rows messages)
  (data-changed! messages (q+:make-qmodelindex)))


;;;---------------------------------------------------
;;;
;;;---------------------------------------------------

(defun push-message (message)
  (trivial-utilities:aif (car (member (source-id message) (known-devices known-devices) :key #'id))
			 (progn
			   (unless (eq (state it) :online)
			     (update-device it :state :online))
			   (when (eq 210 (message-id message))
			     (update-device it :serial-number (value message))))
			 (let ((id (source-id message))
			       (sn (if (eq 210 (message-id message)) (value message) nil))
			       (state (if (eq 208 (message-id message)) :online :probably-online)))
			   (add-device (make-instance 'device
						:id id
						:serial-number sn
						:dev-type 0
						:state state
						:description "Unknown"))))

  (add-message message))

